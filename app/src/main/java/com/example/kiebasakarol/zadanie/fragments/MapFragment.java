package com.example.kiebasakarol.zadanie.fragments;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.kiebasakarol.zadanie.R;
import com.example.kiebasakarol.zadanie.activities.DetailsActivity;
import com.example.kiebasakarol.zadanie.model.Result;
import com.example.kiebasakarol.zadanie.network.Ready4sAPI;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmQuery;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private GoogleMap mMap;

    private double latOfDevice;
    private double lngofDevice;

    Realm realm = Realm.getDefaultInstance();

    /* keeps all result object from API */
    private List<Result> results;
    /* keeps all LatLng object created based on data from Result object */
    private List<LatLng> latlangs;
    /* keeps all markers to give listener */
    private List<Marker> markers;

    private LocationManager manager;

    public static MapFragment newInstance() {
        MapFragment mapFragment = new MapFragment();
        return mapFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
        }
        ConnectivityManager connectivityManager = (ConnectivityManager)
                getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected() && manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            loadData();
        } else {
            buildAlertMessageNoInternet();
        }
    }

    private void buildAlertMessageNoInternet() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage(R.string.no_internet_connection)
                .setCancelable(false)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage(R.string.no_gps)
                .setCancelable(false)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private void loadData() {
        getLatAndLngOfDevice();

        Ready4sAPI.Factory.getIstance().getResults(latOfDevice, lngofDevice).enqueue(new Callback<List<Result>>() {
            @Override
            public void onResponse(Call<List<Result>> call, Response<List<Result>> response) {
                if (response.isSuccessful()) {
                    results = new ArrayList<>();
                    results.addAll(response.body());
                    addMarkresToMap();

                } else {
                    Log.e("error", "blad on response");
                }
            }

            @Override
            public void onFailure(Call<List<Result>> call, Throwable t) {
                Log.e("error", "blad on failure" + t);
            }
        });
    }

    private void getLatAndLngOfDevice() {
        try {
            Location location = manager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            if (location != null) {
                lngofDevice = location.getLongitude();
                latOfDevice = location.getLatitude();
            }
        } catch (SecurityException e) {
            Log.e("error", e.getMessage());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);
        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMarkerClickListener(this);
        try {
            mMap.setMyLocationEnabled(true);
        } catch (SecurityException e) {
            Log.e("error", e.getMessage());
        }
    }

    private void addMarkresToMap() {
        latlangs = new LinkedList<>();
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        markers = new ArrayList<>();

        for (int i = 0; i < results.size(); i++) {
            latlangs.add(new LatLng(results.get(i).getLat(), results.get(i).getLng()));
        }
        for (int i = 0; i < latlangs.size(); i++) {
            MarkerOptions mo = new MarkerOptions().position(latlangs.get(i));

            if (mMap != null)
                markers.add(mMap.addMarker(mo));

            builder.include(mo.getPosition());
        }

        LatLngBounds bounds = builder.build();

        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        int padding = (int) (width * 0.1);
        if (mMap != null) {
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
            mMap.animateCamera(cu);
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        for (Marker tmp : markers) {
            if (tmp.equals(marker)) {
                int index = markers.indexOf(tmp);
                Result result = results.get(index);

                loadDataAndStartDetailsActivity(result);

                addResultToDataBase(result);
            }
        }
        return false;
    }

    private void addResultToDataBase(Result result) {
        if (checkIfExists(result.getId())) {
            realm.beginTransaction();
            realm.copyToRealm(result);
            realm.commitTransaction();
        }
    }

    private boolean checkIfExists(String value) {
        RealmQuery<Result> query = realm.where(Result.class)
                .equalTo("id", value);

        if (query.count() == 0) return true;
        else return false;
    }

    private void loadDataAndStartDetailsActivity(Result result) {
        Intent toDetail = new Intent(getContext(), DetailsActivity.class);
        toDetail.putExtra("infoResult", result);
        getContext().startActivity(toDetail);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        realm.close();
    }
}
