package com.example.kiebasakarol.zadanie.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.kiebasakarol.zadanie.R;
import com.example.kiebasakarol.zadanie.activities.DetailsActivity;
import com.example.kiebasakarol.zadanie.model.Result;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by Kiełbasa Karol on 04.03.2017.
 */

public class LastViewedAdapter extends RecyclerView.Adapter<LastViewedAdapter.MyViewHolder> {

    private Context context;
    Realm realm = Realm.getDefaultInstance();

    public LastViewedAdapter(Context context)
    {
        this.context = context;
    }


    public class MyViewHolder extends  RecyclerView.ViewHolder{
        @BindView(R.id.name_place_lastviewed_tv) TextView namePlaceTextView;
        @BindView(R.id.lat_and_lng_tv) TextView latAndLngTextView;
        @BindView(R.id.imageView) ImageView imageView;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        public void loadData() {
            if(realm.where(Result.class).findAll().size() >0)
            {
                namePlaceTextView.setText( getRealmResult().get(getAdapterPosition()).getName());
                latAndLngTextView.setText(formatLatAndLng());

                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Result result =  getRealmResult().get(getAdapterPosition());
                        loadDataAndStartDetailsActivity(result);
                    }
                });
            }
        }
        private void loadDataAndStartDetailsActivity(Result result) {
            Intent toDetail = new Intent(itemView.getContext(), DetailsActivity.class);
            toDetail.putExtra("infoResult",result);
            itemView.getContext().startActivity(toDetail);
        }

        private String formatLatAndLng() {
            String lngToFormat = getRealmResult().get(getAdapterPosition()).getLng() +"";
            lngToFormat = lngToFormat.substring(0,5);
            String latToFormat = getRealmResult().get(getAdapterPosition()).getLat() +"";
            latToFormat = latToFormat.substring(0,5);
            String latAndLng = latToFormat +"    "+ lngToFormat;
            return latAndLng;
        }
    }

    @Override
    public LastViewedAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int layoutID = R.layout.last_vieved_layout ;

        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(layoutID,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(LastViewedAdapter.MyViewHolder holder, int position) {
        holder.loadData();
    }

    @Override
    public int getItemCount() {
        return realm.where(Result.class).findAll().size();
    }

    private RealmResults<Result> getRealmResult(){
        return realm.where(Result.class).findAll();
    }
}
