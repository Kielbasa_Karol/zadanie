package com.example.kiebasakarol.zadanie.network;

import com.example.kiebasakarol.zadanie.model.Result;

import java.util.List;

import retrofit2.Call;
import retrofit2.GsonConverterFactory;
import retrofit2.Retrofit;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Kiełbasa Karol on 03.03.2017.
 */

public interface Ready4sAPI {

    String BASE_URL = "https://interview-ready4s.herokuapp.com/";

    @GET("/geo")
    Call<List<Result>> getResults(@Query("lat") double lat, @Query("lng") double lng);

    class Factory {
        private static Ready4sAPI service;

        public static Ready4sAPI getIstance() {
            if (service == null) {
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                service = retrofit.create(Ready4sAPI.class);
                return service;
            } else {
                return service;
            }
        }
    }
}
