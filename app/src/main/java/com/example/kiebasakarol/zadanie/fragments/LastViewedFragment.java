package com.example.kiebasakarol.zadanie.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.kiebasakarol.zadanie.R;
import com.example.kiebasakarol.zadanie.adapters.LastViewedAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LastViewedFragment extends Fragment {

    @BindView(R.id.last_viewed_recycle_view) RecyclerView recyclerView;
    private LastViewedAdapter lastViewedAdapter;

    public static LastViewedFragment newInstance() {
        LastViewedFragment lastViewedFragment = new LastViewedFragment();
        return lastViewedFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_last_viewed, container, false);
        ButterKnife.bind(this, view);

        initRecyclerViewAndAdapter();

        return view;
    }

    private void initRecyclerViewAndAdapter() {
        lastViewedAdapter = new LastViewedAdapter(this.getContext());
        lastViewedAdapter.notifyDataSetChanged();
        LinearLayoutManager layoutManager = new LinearLayoutManager(this.getContext());
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(lastViewedAdapter);
        recyclerView.setHasFixedSize(true);
    }

    @Override
    public void onPause() {
        super.onPause();
        lastViewedAdapter.notifyDataSetChanged();
    }
}
