package com.example.kiebasakarol.zadanie.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.kiebasakarol.zadanie.R;
import com.example.kiebasakarol.zadanie.model.Result;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailsActivity extends AppCompatActivity {

    @BindView(R.id.img_iv) ImageView imageView;
    @BindView(R.id.lat_pos_tv) TextView latitudeTextView;
    @BindView(R.id.lng_pos_tv) TextView longitudeTextView;
    @BindView(R.id.place_name_tv) TextView placeNameTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        ButterKnife.bind(this);

        loadDataFromPreviousActivity();
    }

    private void loadDataFromPreviousActivity() {
        Intent previousIntent = getIntent();
        if (previousIntent != null) {
            Bundle data  = getIntent().getExtras();
            Result result = (Result) data.getParcelable("infoResult");

            if(previousIntent.hasExtra("infoResult")){

                latitudeTextView.setText(latFormated(result));
                longitudeTextView.setText(lngFormated(result));
                placeNameTextView.setText(result.getName());


                ConnectivityManager connectivityManager = (ConnectivityManager)
                        getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

                if (networkInfo != null && networkInfo.isConnected()) {
                    Picasso.with(this).load(result.getAvatar()).into(imageView);
                }else
                {
                    buildAlertMessageNoInternet();
                }
            }
        }
    }

    private void buildAlertMessageNoInternet() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.no_internet_connection)
                .setCancelable(false)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private String lngFormated(Result result) {
        String lngToFormat = result.getLng() + " ";
        lngToFormat = lngToFormat.substring(0,5);
        return lngToFormat;
    }

    private String latFormated(Result result) {
        String latToFormat = result.getLat() +" ";
        latToFormat =latToFormat.substring(0,5);
        return latToFormat;
    }
}
