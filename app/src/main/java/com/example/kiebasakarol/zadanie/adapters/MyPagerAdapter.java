package com.example.kiebasakarol.zadanie.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.kiebasakarol.zadanie.fragments.LastViewedFragment;
import com.example.kiebasakarol.zadanie.fragments.MapFragment;

/**
 * Created by Kiełbasa Karol on 02.03.2017.
 */

public class MyPagerAdapter extends FragmentPagerAdapter {
    private String[] strings = {"Mapa" , "Ostatnio"};

    public MyPagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    @Override
    public int getCount() {
        return strings.length;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return MapFragment.newInstance();
            case 1:
                return LastViewedFragment.newInstance();
            default:
                return null;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return strings[position];
    }
}